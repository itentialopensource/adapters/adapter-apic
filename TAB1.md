# Overview 

This adapter is used to integrate the Itential Automation Platform (IAP) with the Apic System. The API that was used to build the adapter for Apic is usually available in the report directory of this adapter. The adapter utilizes the Apic API to provide the integrations that are deemed pertinent to IAP. The ReadMe file is intended to provide information on this adapter it is generated from various other Markdown files.

## Details 
The APIC adapter from Itential is used to integrate the Itential Automation Platform (IAP) with APIC to optimize performance and manage and operate a scalable multitenant Cisco ACI fabric. With this adapter you have the ability to perform operations such as:

- Create Tenant
- Delete Tenant
- Create Tenant Application Profile
- Create Tenant Application Profile EPG

For further technical details on how to install and use this adapter, please click the Technical Documentation tab. 
