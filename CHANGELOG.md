
## 0.10.5 [10-15-2024]

* Changes made at 2024.10.14_21:26PM

See merge request itentialopensource/adapters/adapter-apic!22

---

## 0.10.4 [08-22-2024]

* update dependencies and metadata

See merge request itentialopensource/adapters/adapter-apic!20

---

## 0.10.3 [08-14-2024]

* Changes made at 2024.08.14_19:44PM

See merge request itentialopensource/adapters/adapter-apic!19

---

## 0.10.2 [08-07-2024]

* Changes made at 2024.08.06_21:48PM

See merge request itentialopensource/adapters/adapter-apic!18

---

## 0.10.1 [08-06-2024]

* Changes made at 2024.08.06_09:58AM

See merge request itentialopensource/adapters/adapter-apic!17

---

## 0.10.0 [05-16-2024]

* Minor/2024 auto migration

See merge request itentialopensource/adapters/security/adapter-apic!16

---

## 0.9.7 [03-27-2024]

* Changes made at 2024.03.27_13:53PM

See merge request itentialopensource/adapters/security/adapter-apic!15

---

## 0.9.6 [03-21-2024]

* Changes made at 2024.03.21_14:49PM

See merge request itentialopensource/adapters/security/adapter-apic!14

---

## 0.9.5 [03-13-2024]

* fix vendor

See merge request itentialopensource/adapters/security/adapter-apic!13

---

## 0.9.4 [03-13-2024]

* Changes made at 2024.03.13_10:56AM

See merge request itentialopensource/adapters/security/adapter-apic!12

---

## 0.9.3 [03-11-2024]

* Changes made at 2024.03.11_16:21PM

See merge request itentialopensource/adapters/security/adapter-apic!11

---

## 0.9.2 [02-27-2024]

* Changes made at 2024.02.27_11:57AM

See merge request itentialopensource/adapters/security/adapter-apic!10

---

## 0.9.1 [12-24-2023]

* update axios and metadata

See merge request itentialopensource/adapters/security/adapter-apic!9

---

## 0.9.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/security/adapter-apic!8

---

## 0.8.0 [12-14-2023]

* More migration changes

See merge request itentialopensource/adapters/security/adapter-apic!8

---

## 0.7.0 [11-06-2023]

* More migration changes

See merge request itentialopensource/adapters/security/adapter-apic!8

---

## 0.6.0 [10-19-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-apic!7

---

## 0.5.0 [09-20-2023]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/security/adapter-apic!7

---

## 0.4.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-apic!6

---

## 0.3.4 [02-26-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/security/adapter-apic!5

---

## 0.3.3 [07-05-2020]

- Update to the latest foundation

See merge request itentialopensource/adapters/security/adapter-apic!4

---

## 0.3.2 [01-16-2020] & 0.3.1 [12-31-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/security/adapter-apic!3

---

## 0.3.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/security/adapter-apic!2

---

## 0.2.0 [09-10-2019]

- September migration to bring the adapter up to the latest foundation

See merge request itentialopensource/adapters/security/adapter-apic!1

---
## 0.1.3 [07-30-2019] & 0.1.2 [07-19-2019] & 0.1.1 [07-19-2019]

- Initial Commit

See commit 94cf59a

---
