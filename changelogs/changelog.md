
## 0.4.0 [05-27-2022]

* Migration to the latest Adapter Foundation

See merge request itentialopensource/adapters/security/adapter-apic!6

---

## 0.3.4 [02-26-2021]

- Migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/security/adapter-apic!5

---

## 0.3.3 [07-05-2020]

- Update to the latest foundation

See merge request itentialopensource/adapters/security/adapter-apic!4

---

## 0.3.2 [01-16-2020] & 0.3.1 [12-31-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/security/adapter-apic!3

---

## 0.3.0 [11-07-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/security/adapter-apic!2

---

## 0.2.0 [09-10-2019]

- September migration to bring the adapter up to the latest foundation

See merge request itentialopensource/adapters/security/adapter-apic!1

---
## 0.1.3 [07-30-2019] & 0.1.2 [07-19-2019] & 0.1.1 [07-19-2019]

- Initial Commit

See commit 94cf59a

---
